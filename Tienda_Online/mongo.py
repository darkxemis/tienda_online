
# local.py
from .base import *

DEBUG = True
ALLOWED_HOSTS = []

#Database de mongodb en un servidor llamado: https://mlab.com/
#https://docs.mongodb.com/v3.2/tutorial/install-mongodb-on-windows/
DATABASES = {
    'default': {
        'ENGINE': 'djongo',
        'NAME': 'TiendaOnline',
        'HOST': 'mongodb+srv://josemi:josemi@cluster0-e5bwb.mongodb.net/test?retryWrites=true&w=majority',
        'USER': 'josemi',
        'PASSWORD': 'josemi',

    }
}

STATIC_ROOT = os.path.join(BASE_DIR, 'static/root')
#STATICFILES_FINDERS = ('django.contrib.staticfiles.finders.FileSystemFinder', 'django.contrib.staticfiles.finders.AppDirectoriesFinder', 'django.contrib.staticfiles.finders.AppDirectoriesFinder',)

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

STATIC_URL = "/static/"

MEDIA_URL = '/media/'


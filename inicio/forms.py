from django import forms

from .models import Articulo

class PostForm(forms.Form):
	nombre = forms.CharField(max_length=20, widget=forms.HiddenInput())
	precio = forms.DecimalField(max_digits=10, decimal_places=2, widget=forms.HiddenInput())
	foto = forms.CharField(max_length=200, widget=forms.HiddenInput())

	
	#form.fields['nombre'].widget = forms.HiddenInput()
	

    #class Meta:
    #   model = Articulo
    #    fields = ('nombre', 'precio')
    #    widgets = {'nombre': forms.HiddenInput()}
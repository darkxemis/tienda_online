from django.shortcuts import render, get_object_or_404, redirect

from inicio.models import *
from .forms import *

from django.http import HttpResponse
import json



def index(request):
	categorias = Categoria.objects.all()
	articuloimagen = Articulo.objects.all().prefetch_related('imagenes_articulos').order_by('nombre')
	
	request.session['currency'] = '€'
	#Si en la sessión del navegador es nueva o otra, inicializaremos a 0 el carrito
	if 'cart_items' not in request.session:
		request.session['cart_items'] = [{
				'total_price': '0',
                'total_items': '0'
			}]
		request.session['total_price'] = 0
		request.session['total_items'] = 0

	total_items_cart = request.session['total_items'] #Es el número total de items en la cesta

	return render(request, 'index.html', locals())

def detail(request, articulo_id):
    articulo = get_object_or_404(Articulo, pk=articulo_id)

    qs = ArticuloImagen.objects.filter(articulo_id=articulo_id)
    first_image = qs[0].foto

    total_items_cart = request.session['total_items'] #Es el número total de items en la cesta

    #form = PostForm() //Si queremos activar el formalio de forms.py

    return render(request, 'detail.html', locals())

def categoriaId(request, categoria_id):
    articuloimagen = Articulo.objects.filter(categoria = categoria_id)
    categorias = Categoria.objects.all()
    total_items_cart = request.session['total_items'] #Es el número total de items en la cesta


    return render(request, 'categoria.html', locals())

def busqueda(request):
	if request.is_ajax(): 
		articuloimagen = Articulo.objects.filter(nombre__startswith=request.GET['nombre']).values('nombre', 'id').order_by('-nombre')
		json_articulos_list = list(articuloimagen) 
		
		i = 0
		#Creación del json por cada resultado obtenido en la query anterior
		for articulo_json in json_articulos_list:
			
			articulo = Articulo.objects.filter(id=articulo_json['id'])
			precio = articulo[0].precio

			id_articulo_foto = Articulo.objects.filter(id=articulo_json['id']).values_list('imagenes_articulos', flat=True).first()
			path_imagen = ArticuloImagen.objects.filter(id=int(id_articulo_foto)).values('foto').first()

			json_articulos_list[i]['imagen'] = path_imagen
			json_articulos_list[i]['precio'] = str(precio)

			i = i + 1
	
		print(json_articulos_list)
		return HttpResponse( json.dumps(json_articulos_list), content_type='application/json' )
		#return render(request, 'busqueda.html', locals())
	elif request.method == 'GET':
		print(request.GET['category_list'])
		print(request.GET['search'])
		return redirect('index')


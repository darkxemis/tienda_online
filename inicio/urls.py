from django.urls import path

from inicio import views

urlpatterns = [
	path('', views.index, name='index'),
	path('articulo/<int:articulo_id>/', views.detail, name='detail'),
	path('categoria/<int:categoria_id>/', views.categoriaId, name='categoriaId'),
	path('inicio/busqueda', views.busqueda, name='busqueda'), 
	#path('inicio/deleteItem', views.deleteItemCart, name='deleteItemCart'),
]
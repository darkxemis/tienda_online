(function($) {
	"use strict"

	// Mobile Nav toggle
	$('.menu-toggle > a').on('click', function (e) {
		e.preventDefault();
		$('#responsive-nav').toggleClass('active');
	})

	// Fix cart dropdown from closing
	$('.cart-dropdown').on('click', function (e) {
		e.stopPropagation();
	});

	$('#carrito').on('click', function (e) {
		//FUNCION AJAX QUE ME DA LOS ITEMS DEL CARRITO

		$.ajax({
			url: 'http://' + window.location.host + '/cart/loadCart',
			type: 'GET',
			success : function(data) {

				$("#productos_carrito").empty(); //Eliminamos el contenido del div productos_carrito
		    	$("#productos_resumen > ").empty(); //Eliminamos el contenido del div productos_resumen

				$.fn.createCartView(data);
			},
			error : function(message) {
		        console.log(message);
		    }
		});
	});

	$.fn.createCartView = function(data_json) { 
        $.each(data_json, function(i, obj) {
			if(obj.total_items == null) {
				$("#productos_carrito").prepend("<div>" + "</div>");
				$("#productos_carrito > div:first").addClass("product-widget").attr('id', 'product_widget' + i);
				
				$("#product_widget" + i).prepend("<div>" + "</div>");
				/*Creamos un input box oculto para meter el id que posteriormente usaremos en la funcion eliminar item*/
				$("#product_widget" + i).prepend("<input>");
				$("#product_widget" + i + '> input').val(obj.id);
				$("#product_widget" + i + '> input').attr('type', 'hidden');
				$("#product_widget" + i + '> input').attr('id', 'id_input');
				

				$("#product_widget" + i + " > div").addClass("product-img").attr('id', 'product_img' + i);

				$("#product_img" + i).prepend("<img>");
				$("#product_img" + i + " > img").attr({
													src:"/media/" + obj.photo,
													alt:"producto",
													width:"40px",
													height:"40px"
												});

				$("#product_widget" + i).append("<div>" + "</div>");
				$("#product_widget"+ i + " > div:last").addClass("product-body").attr('id', 'product_body' + i);

				$("#product_body" + i).prepend("<h3>" + "</h3>");
				$("#product_body" + i + " > h3").addClass("product-name")
				$("#product_body" + i + " > h3").prepend("<a>" + obj.name + "</a>"); 
				$("#product_body" + i + " > h3 > a").attr('href', '../../articulo/' + obj.id); 

				$("#product_body" + i).prepend("<h4>" + obj.price + "</h4>");
				$("#product_body" + i + " > h4").addClass("product-price")
				$("#product_body" + i + " > h4").prepend("<span> 1x </span>"); 
				$("#product_body" + i + " > h4 > spam").addClass('qty');


				/*Botón de eliminar de la cesta en el dropdown*/
				$("#product_widget" + i).append("<button><i> </i></button>");
				$("#product_widget" + i + " > button").addClass("delete");
				//$("#product_widget" + i + " > a").attr("href", "#"); 
				$("#product_widget" + i + " > button").attr("id", "elimin"); 
				$("#product_widget" + i + " > button > i").addClass("fa fa-close");

				/*Añadir el evento para eliminar el item de la cesta en la vista*/
				$('#elimin').on('click', function() {

					var id = $(this).parent().find('#id_input').val()
					var url = 'http://' + window.location.host + '/cart/deleteItemCart';

					$.ajax({
					data: {'id': id},
					url: url,
					type: 'GET',
					success : function(data) {
						$("#productos_carrito").empty(); //Eliminamos el contenido del div productos_carrito
		    			$("#productos_resumen > ").empty(); //Eliminamos el contenido del div productos_resumen

						$.fn.createCartView(data);
					},
					error : function(message) {
			        	console.log(message);
					}
					});
				});
			}
		});

		$("#productos_resumen > small").text(data_json[data_json.length -1].total_items + " Item(s) selected");
		$("#productos_resumen > h5").text("SUBTOTAL: " + data_json[data_json.length -1].total_price);
		$("#total_items_cart").text(data_json[data_json.length -1].total_items);
    }

	$('.input').keyup(function(e){
		var consulta = $(".input").val();
		var category_list = $("#category_list").val();

		$.ajax({
		data: {'nombre': consulta, 'category_list': category_list},
		url: 'http://' + window.location.host + '/inicio/busqueda',
		type: 'GET',
		success : function(data) {
	 		if (data.length > 0) {
	 			$.fn.createSearchedItem(data)
			} else {
				$("#contenedor_producto").empty(); //Eliminamos el contenido del div contenedor_producto
				//Crear un mensajeto de que no hay mas busquedas
			}      
		},
		error : function(message) {
	         console.log(message);
	      }
		});
	});	

	$.fn.createSearchedItem = function(data_json) {
		$("#contenedor_producto").empty(); //Eliminamos el contenido del div contenedor_producto

        $.each(data_json, function(i, obj) {
			$("#contenedor_producto").prepend("<div>" + "</div>");
			$("#contenedor_producto > div").addClass("col-md-4 col-xs-6").attr('id', 'ejemplo' + i);

			//Creación del div con la clase shop que contiene la foto y el texto
			$("#ejemplo" + i).prepend("<div>" + "</div> ")
			$("#ejemplo" + i + " > div").addClass("shop");
			$("#ejemplo" + i + " > div").attr("id", "shop" + i);

			//Creación de los dos div para separar la imagen del texto
            $("#shop" + i).prepend("<div>" + "</div> <div>" + "</div>")
            
            $("#shop" + i + " > div:first").prepend("<img>").attr('id', 'shop_img' + i);
            $("#shop_img" + i).addClass('shop-img')
            $("#shop_img" + i + " > img").attr({
												    src:"/media/" + obj.imagen['foto'], 
												    alt:"foto",
												    width:"210px",
												    height:"200px",
												    style:"float: right"
												});

            //.attr("src", "/media/" + obj.imagen['foto']);

        	$("#shop" + i + " > div:last").attr('id', 'shop_body' + i);

        	$("#shop_body" + i).prepend('<a> </a>')
        	$("#shop_body" + i + " > a").prepend('shop now <i> </i>').addClass('cta-btn');
        	//$("#shop_body > a").val('shop now');
        	$("#shop_body" + i + " > a > i").addClass('fa fa-arrow-circle-right');

        	$("#shop_body" + i + " > a").attr("href", '../../articulo/' + obj.id)//'{% url ' + some+ ' ' + 6 + '%}');

        	$("#shop_body" + i).prepend("<h3>" + obj.nombre + "<br>" + obj.precio + "</h3>").addClass('shop-body');
		});
	};

	//Establece en el campo input el focus constantemente que se recarga la página
	$(document).ready(function(){ 
		var searchInput = $('.input');
		var strLength = searchInput.val().length * 2;

		searchInput.focus();
		searchInput[0].setSelectionRange(strLength, strLength);
	 });

	/////////////////////////////////////////

	// Products Slick
	$('.products-slick').each(function() {
		var $this = $(this),
				$nav = $this.attr('data-nav');

		$this.slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			autoplay: true,
			infinite: true,
			speed: 300,
			dots: false,
			arrows: true,
			appendArrows: $nav ? $nav : false,
			responsive: [{
	        breakpoint: 991,
	        settings: {
	          slidesToShow: 2,
	          slidesToScroll: 1,
	        }
	      },
	      {
	        breakpoint: 480,
	        settings: {
	          slidesToShow: 1,
	          slidesToScroll: 1,
	        }
	      },
	    ]
		});
	});

	// Products Widget Slick
	$('.products-widget-slick').each(function() {
		var $this = $(this),
				$nav = $this.attr('data-nav');

		$this.slick({
			infinite: true,
			autoplay: true,
			speed: 300,
			dots: false,
			arrows: true,
			appendArrows: $nav ? $nav : false,
		});
	});

	/////////////////////////////////////////

	// Product Main img Slick
	$('#product-main-img').slick({
    infinite: true,
    speed: 300,
    dots: false,
    arrows: true,
    fade: true,
    asNavFor: '#product-imgs',
  });
	
	// Product imgs Slick
  $('#product-imgs').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    centerMode: true,
    focusOnSelect: true,
		centerPadding: 0,
		vertical: true,
    asNavFor: '#product-main-img',
		responsive: [{
        breakpoint: 991,
        settings: {
					vertical: false,
					arrows: false,
					dots: true,
        }
      },
    ]
  });

	// Product img zoom
	var zoomMainProduct = document.getElementById('product-main-img');
	if (zoomMainProduct) {
		$('#product-main-img .product-preview').zoom();
	}

	/////////////////////////////////////////

	// Input number
	$('.input-number').each(function() {
		var $this = $(this),
		$input = $this.find('input[type="number"]'),
		up = $this.find('.qty-up'),
		down = $this.find('.qty-down');

		down.on('click', function () {
			var value = parseInt($input.val()) - 1;
			value = value < 1 ? 1 : value;
			$input.val(value);
			$input.change();
			updatePriceSlider($this , value)
		})

		up.on('click', function () {
			var value = parseInt($input.val()) + 1;
			$input.val(value);
			$input.change();
			updatePriceSlider($this , value)
		})
	});

	//var priceInputMax = document.getElementById('price-max'),
	//		priceInputMin = document.getElementById('price-min');
	var priceInputMax = document.getElementById('1000000'),
		priceInputMin = document.getElementById('0');
	
	function updatePriceSlider(elem , value) {
		if ( elem.hasClass('price-min') ) {
			console.log('min')
			priceSlider.noUiSlider.set([value, null]);
		} else if ( elem.hasClass('price-max')) {
			console.log('max')
			priceSlider.noUiSlider.set([null, value]);
		}
	}

	priceInputMax.addEventListener('change', function(){
		updatePriceSlider($(this).parent() , this.value)
	});

	priceInputMin.addEventListener('change', function(){
		updatePriceSlider($(this).parent() , this.value)
	});

	// Price Slider
	var priceSlider = document.getElementById('price-slider');
	if (priceSlider) {
		noUiSlider.create(priceSlider, {
			start: [1, 999],
			connect: true,
			step: 1,
			range: {
				'min': 1,
				'max': 999
			}
		});

		priceSlider.noUiSlider.on('update', function( values, handle ) {
			var value = values[handle];
			handle ? priceInputMax.value = value : priceInputMin.value = value
		});
	}

})(jQuery);

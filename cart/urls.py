from django.urls import path

from cart import views

urlpatterns = [
	path('cart/loadCart', views.loadCart, name='loadCart'),
	#path('cart/addItemCart', views.addItemCart, name='addItemCart'),
	path('cart/addItemCart', views.addItemCart, name='addItemCart'),
	path('cart/deleteItemCart', views.deleteItemCart, name='deleteItemCart'),
]
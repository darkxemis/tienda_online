from django.shortcuts import render, redirect

from django.http import HttpResponse

import json
from decimal import Decimal


def loadCart(request):
	if request.is_ajax(): 
		return HttpResponse( json.dumps(request.session['cart_items']), content_type='application/json' )

def addItemCart(request):
	if request.method == 'POST':
		#del request.session['cart_items'] #descomentar para limpiar la cesta para pruebas
		#del request.session['total_price']
		#del request.session['total_items']
		#request.session['total_price'] = 0
		#request.session['total_items'] = 0

		#Obtenemos el precio del nuevo item y lo parseamos para después calcularlo
		price = round(Decimal(request.POST['price'].replace('€', '')), 2) 
		
		items_in_cart = request.session['cart_items']

		items = {
				'id': request.POST['id'],
                'name': request.POST['name'],
                'price': request.POST['price'],
                'photo': request.POST['photo']
            }

		#Cargamos el precio total y el número total de items en el carrito en el json
		cart_items_size = len(request.session['cart_items']) -1
		last_total_price = Decimal(request.session['cart_items'][cart_items_size]['total_price'].replace('€', ''))
		new_total_price = last_total_price + price

		request.session['total_items'] += 1 #Sumamos uno a la variable global de total_items
		
		request.session['cart_items'][cart_items_size]['total_price'] = str(new_total_price) + ' ' + request.session['currency'] 
		request.session['cart_items'][cart_items_size]['total_items'] = request.session['total_items']
		
		items_in_cart.insert(0, items) #Insertamos el siguiente objeto al principio de los items
		request.session['cart_items'] = items_in_cart
		
	return redirect('index') #Devolveremos lo que devuelve la función que carga el index [ inicio>views.py ]

def	deleteItemCart(request):
	json_cart = request.session['cart_items']

	for i in range(len(json_cart)):
		if json_cart[i]["id"] == request.GET['id']:
			#Restamos el valor del producto para recalcular el total
			price_to_subtrac = round(Decimal(json_cart[i]["price"].replace('€', '')), 2)
			total_price = Decimal(json_cart[len(json_cart) -1]['total_price'].replace('€', ''))
			json_cart[len(json_cart) -1]['total_price'] = str(total_price - price_to_subtrac) + ' €'
			#Seteamos también la variable global para evitar que cambie el estado cuando abro de nuevo el carrito
			request.session['total_price'] = str(total_price - price_to_subtrac) + ' €'

			#Restamos uno al valor total de los items
			total_items = json_cart[len(json_cart) -1]['total_items']
			json_cart[len(json_cart) -1]['total_items'] = int(total_items) - 1
			#Seteamos también la variable global para evitar que cambie el estado cuando abro de nuevo el carrito
			request.session['total_items'] -= 1

			#Eliminamos el elemento ya recalculado todo
			json_cart.pop(i)

			request.session['cart_items'] = json_cart
			break

	return HttpResponse( json.dumps(request.session['cart_items']), content_type='application/json' )

